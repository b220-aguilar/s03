# Task 1 - Car Dictionary
car = {
	'brand': 'Toyota',
	'model': 'Veloz',
	'year_of_make': '2022',
	'color': 'Pearl White'
}

print(f"I own a {car['color']} {car['brand']} {car['model']} and it was made in {car['year_of_make']}")


# Task 2 - Square function
def square(num):
	return num*num

print(square(9))


#  Task 3 - Translate function "Hello World"
def translate(lang):
	trn_dict = {
		"French":"Bonjour le monde",
		"Japanese": "Konichiwa sekai",
		"Spanish": "Hola Mundo"
	}

	if(lang in trn_dict):
		print(trn_dict[lang])
	else:
		print("Invalid Language")


translate("Korean")
translate("Spanish")