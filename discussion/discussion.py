# [Section] Lists
# lists in python is similar to the Arrays in JS. They both provide a collection of data.
names = ["John", "Paul", "George", "Ringo"]
programs = ['developer career', 'pi-shape', 'short courses']
durations = [ 360, 180, 20]
truth_values = [True, False, True, True, False ]

print(names)

# list with different data types
sample_list = [ "Apple", 3, False, 'Potato', 4, True ] # this is allowed in Python. Just keep in mind the convention of having the same data type in each of the list
print(sample_list)

# getting the size of a list
print(len(programs))
# print(len("Apple")) it can also be used in strings

# accessing the values inside the lists
print(durations[1])
print(programs[0])
# since python has negative indeces, we can commonly use it to check the last elemtn of the list
print(durations[-1]) # last element
print(names[-3]) # third element from the end of the list

# accessing non-existent indeces
# print(truth_values[20]) # would return an error: index out of range

# Range of indeces
print(names[0:2]) # from index 0, print elements until before index 2


# [Section] MINIACTIVITY
"""
1. Create a list of names of 5 students
2. Create a list of grades for the 5 students
3. Using a loop, iterate through the lists and print "The grade of <student> is <grade>"

	
"""

students = [ "Luke", "Leia", "Chewie", "Yoda", "Obiwan"]
grades = [ 95, 91, 89, 89, 97]

for x in range(5):
	print(f"The grade of {students[x]} is {grades[x]}")



# Updating Lists
# print current value
print(f'Current value: {names[2]}')


# update the value
names[2] = 'Michael'

# print the new value
print(f'Current value: {names[2]}')

# List Manipulation
# Lists have methods that can be used to manipulate the elements within

# Adding List Items - the append() method allows inserting items to the end of the list

names.append('Bobby')
print(names)

# Deleting List Items - the 'del' keyword can be used to delete items form a list

durations.append(280)
print(durations)

# Delete the last item from 'durations'
del durations[-1]
print(durations)


# Membership checks - the 'in' keyword checks if a given element is in the list and returns True or False

print(20 in durations)
print(500 in durations)


# Sorting Lists - the sort() method sorts the lists alphanumerically in ascending order by default

names.sort()
print(names)


# Looping through Lists
count = 0
while count < len(names):
	print(names[count])
	count += 1

print('---')

for x in range(len(names)):
	print(names[x])


# [Section] Dictionaries
# Dictionaries are used to store data in key:value pairs, similar to Objects in JS. 
# They are collections which are ordered, changeable and do not allow duplicates

# Ordered - items have a defined order that cannot be changed
# Changeable - values can be changed
person1 = {
	"name": "Brandon",
	"age": 28,
	"occupation": "student",
	"isEnrolled": True,
	"subjects": ["Python", "SQL", "Django"]
}

# To get the number of key:value pairs in a dictionary, the len() method can be used again
print(len(person1))

# Accessing values in Dictionaries
# To get the value of an item in a dictionary, the keyname can be used with a pair of []

print(person1['name'])

# The keys() and values() methods will return a lst of all keys/values in the dictionary
print(person1.keys())
print(person1.values())


# The items() method will return each item in the dictionary as a key:value pair in a list
print(person1.items())

# Adding key:value pairs can be done by either putting a new index key and assigning a value or by using the update() method

person1['nationality'] = 'Filipino'
person1.update({'fav_food':'BBQ'})
print(person1)


# Deleting entries can be done using the pop() method or the del keyword

person1.pop('fav_food')
del person1['nationality']
print(person1)


# Looping through dictionaries
for key in person1 :
	print(f"The value of {key} is {person1[key]}")

# Nested Dictionaries

person2 = {
	"name": "Monika",
	"age": 28,
	"occupation": "student",
	"isEnrolled": True,
	"subjects": ["Python", "SQL", "Django"]
}


classRoom = {
	"student1": person1,
	"student2": person2
}

print(classRoom)


# [Section] Functions
# Functions are blocks of code that can be run when called/invoked. A function can be used to get input, process the input, and return an output

# the 'def' keyword is used to create a function

def my_greeting():
	# code to be executed when function is invoked
	print('Hello user!')

# Calling/Invoking the function
my_greeting()

# Parameters can be added to functions to have more control as to what input the function will need
def greet_user(username):
	# prints out the value of the username parameters
	print(f'Hello, {username}!')

# Arguments are the values represented by parameters
greet_user("Rhaenyra")
greet_user("Daemon")

# Return statements - the "return" keyword allows the function to return values, just like in JS

def addition(num1, num2):
	return num1 + num2 

sum = addition(5,10)
print(f"The sum is {sum}")


# Lambda Function
# A lambda function is a small anonymous function that can be used for callbacks. It is just like any normal Python function, except that its name is defined as a variable, and usually contains just one line of code

# A Lambda Function can take any number of arguments, but can only have one expression

greeting = lambda person : f'Hello {person}'
print(greeting("Anthony"))

mult = lambda a, b : a*b 
print(mult(5,6))



def increaser(num1):
	return lambda num2: num2*num1


doubler = increaser(2)

print(doubler(11))

tripler = increaser(3)

print(tripler(7))